﻿/// <summary>
/// FollowPlayer.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>
/// 
using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public Player player;
	public float yOffset = 1;
	public float followDistance = 5;
	public float rotationSpeed = 90f;

	Vector3 lastDirection;

	// Use this for initialization
	void Start () {
		
	}
	
	void LateUpdate () {
		if (player.playerGraphic != null && player.currentWaypoint < player.waypoints.Length) {

			// in which direction is the player moving?
		

			Vector3 newDirection = player.waypoints[player.currentWaypoint].position - player.playerGraphic.transform.position;

			Vector3 direction = Vector3.Lerp(lastDirection, newDirection, Time.deltaTime * 4);

			direction = direction.normalized;

			transform.position = Vector3.Lerp(transform.position, player.playerGraphic.transform.position - direction * followDistance + yOffset * Vector3.up, Time.deltaTime * 4);

			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);

			lastDirection = direction;
		}
	}
}
