﻿/// <summary>
/// AudioManager.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioMixer audioMixer;

	public Slider sfxVolume;
	public Slider musicVolume;

	// Use this for initialization
	void Start () {

		if (PlayerPrefs.HasKey("SFXVolume") && sfxVolume != null) {
			SetSFXVolume(PlayerPrefs.GetFloat("SFXVolume"));
		}

		if (PlayerPrefs.HasKey("MusicVolume") && musicVolume != null) {
			SetMusicVolume(PlayerPrefs.GetFloat("MusicVolume"));
		}
	}

	public void SetSFXVolume(float volume) {
		float newVolume = volume;
		if (volume <= -40) {
			newVolume = -80;
		}
		audioMixer.SetFloat("SFXVolume", newVolume);
		PlayerPrefs.SetFloat("SFXVolume", newVolume);
		sfxVolume.value = newVolume;
	}

	public void SetMusicVolume(float volume) {
		float newVolume = volume;
		if (volume <= -40) {
			newVolume = -80;
		}
		audioMixer.SetFloat("MusicVolume", newVolume);
		PlayerPrefs.SetFloat("MusicVolume", newVolume);
		musicVolume.value = newVolume;
	}




}