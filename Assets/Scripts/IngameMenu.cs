﻿/// <summary>
/// IngameMenu.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class IngameMenu : MonoBehaviour {

	public void ReloadCurrentLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void GoToMainMenu() {
		SceneManager.LoadScene("MainMenu");
	}
}
