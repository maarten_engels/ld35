﻿/// <summary>
/// Player.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Player : MonoBehaviour {

	public float playTime = 0;

	public LayerMask layerMask = -1;
	public Rigidbody playerGraphic;
	public Transform[] waypoints;
	public float speed;
	public float acceleration = 0; // the amount with which speed increases per second;
	public bool isAlive = false;
	public float maxBounceOnChangeHeight = 1f;
	public GameObject[] gibs;
	public GameObject gameEndPanel;
	public Text endgameText;
	public GameObject winEffectPrefab;

	public Rigidbody[] playerGraphicPrefabs;
	int playerGraphicIndex = 0;

	public int currentWaypoint { get; protected set; } 

	public bool debugAutoChange = false;
	public Text countdownText;
	float debugAutoChangeTimer = 0;
	bool isBraking = false;

	System.Action cbGameEnded;

	// Use this for initialization
	void Start () {
		Rigidbody rb = Instantiate(playerGraphicPrefabs[playerGraphicIndex], transform.position, Quaternion.identity) as Rigidbody;
		playerGraphic = rb;

		countdownText.enabled = false;
		gameEndPanel.SetActive(false);
		StartCoroutine("CountDown");

		//Time.timeScale = 5f;

	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			ChangePlayerGraphic(0);
		}

		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			ChangePlayerGraphic(1);
		}

		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			ChangePlayerGraphic(2);
		}

		if (Input.GetKeyDown(KeyCode.Alpha4)) {
			ChangePlayerGraphic(3);
		}

		isBraking = Input.GetButton("Brake");


		if (PlayerDistanceToWaypoint() < 5f) {
			currentWaypoint++;
			currentWaypoint = currentWaypoint % waypoints.Length;
		}

		if (isAlive) {
			playTime += Time.deltaTime;
		}

		debugAutoChangeTimer -= Time.deltaTime;
		if (debugAutoChange && debugAutoChangeTimer < 0) {
			ChangePlayerGraphic(Random.Range(0, playerGraphicPrefabs.Length));
			debugAutoChangeTimer = Random.Range(0.5f, 1.5f);
		}
 
	}

	float PlayerDistanceToWaypoint() {
		// check whether a player graphic is assigned. We return a relatively large value to make sure that 
		// we don't conclude that we are actually close to a waypoint.
		if (playerGraphic == null) {
			return 1000;
		}

		Vector3 playerPositionXZ = new Vector2(playerGraphic.transform.position.x, playerGraphic.transform.position.z);
		Vector3 waypointPositionXZ = new Vector2(waypoints[currentWaypoint].position.x, waypoints[currentWaypoint].position.z);

		return Vector2.Distance(playerPositionXZ, waypointPositionXZ);
	}

	public void ChangePlayerGraphic(int index) {

		// Check for array index out-of-bounds
		if (index < 0 || index >= playerGraphicPrefabs.Length) {
			Debug.LogError("No playerGraphicPrefab exists for index: "+index);
			return;
		}

		// Check to see whether a new graphic was chosen.
		if (index == playerGraphicIndex) {
			// no new player graphic is chosen, returning;
			return;
		}

		// Check to see whether we are still alive.
		if (isAlive == false) {
			// we are dead and cannot change our graphic
			return;
		}

		playerGraphicIndex = index;

		// Create a new playerGrahpic at the exact position as the old one, including rotation.
		Rigidbody rb = Instantiate(playerGraphicPrefabs[playerGraphicIndex], playerGraphic.transform.position, playerGraphic.transform.rotation) as Rigidbody;

		rb.velocity = playerGraphic.velocity;
		rb.angularVelocity = playerGraphic.angularVelocity;

		// Give a bit of bounce on change
		if (rb.transform.position.y < maxBounceOnChangeHeight) {
			rb.velocity += Vector3.up; 
		}

		// Destroy the old playerGraphic
		Destroy(playerGraphic.gameObject);

		playerGraphic = rb;

	}

	void FixedUpdate() {
		// faster and faster!
		// only increase acceleration when we are not braking to prevent crazy effects.
		if (isBraking == false) {
			speed += acceleration * Time.fixedDeltaTime; 
		}

		if (playerGraphic != null && isAlive == true) {

			// Check whether we are still within the bounds of the level
			if (Physics.Raycast(playerGraphic.position, Vector3.down, 4, layerMask) == false) {
				// we are out of bounds, bail.
				playerGraphic.velocity += Physics.gravity * Time.deltaTime;
				return;
			}

			Vector3 direction = waypoints[currentWaypoint].position - playerGraphic.transform.position;
			direction = direction.normalized;
			if (isBraking == false) {
				playerGraphic.velocity = Vector3.Lerp(playerGraphic.velocity, direction * speed, Time.fixedDeltaTime);
			} else {
				playerGraphic.velocity = Vector3.Lerp(playerGraphic.velocity, Vector3.zero, Time.fixedDeltaTime * 4);
			}
		}


	}

	public void Win() {
		isAlive = false;
		if (cbGameEnded != null) {
			cbGameEnded();
		}

		Instantiate(winEffectPrefab, playerGraphic.transform.position, Quaternion.identity);
		gameEndPanel.SetActive(true);
		endgameText.text = "You Won!";
	}

	public void Die() {
		if (isAlive == false) {
			return;
		}

		isAlive = false;
		if (cbGameEnded != null) {
			cbGameEnded();
		}

		StartCoroutine("DieEffect");
	}

	IEnumerator DieEffect() {
		
		yield return new WaitForSeconds(1.0f);

		if (playerGraphicIndex < gibs.Length && gibs[playerGraphicIndex] != null) {
			Instantiate(gibs[playerGraphicIndex], playerGraphic.transform.position, Quaternion.identity);

		}

		gameEndPanel.SetActive(true);
		endgameText.text = "Game Over";

		Destroy(playerGraphic.gameObject);
	}

	public void RegisterGameEndedCallback(System.Action callback) {
		cbGameEnded += callback;
	}

	IEnumerator CountDown() {

		countdownText.enabled = true;

		// Green / 3
		Color textColor = new Color(0, 118f/255f, 1, 1);
		countdownText.text = "3";

		float t = 1f;
		while (t > 0) {
			float a = Mathf.Lerp(1, 0, t * 2);
			textColor = new Color(textColor.r, textColor.g, textColor.b, a);
			countdownText.color = textColor;
			t -= Time.deltaTime;
			yield return 0;
		} 



		// Blue / 2
		textColor = new Color(41f/255f, 225f/255f, 111f/255f, 1);
		countdownText.text = "2";

		t = 1f;
		while (t > 0) {
			float a = Mathf.Lerp(1, 0, t * 2);
			textColor = new Color(textColor.r, textColor.g, textColor.b, a);
			countdownText.color = textColor;
			t -= Time.deltaTime;
			yield return 0;
		} 

		// Yellow / 1
		textColor = new Color(1, 244f/255f, 98f/255f, 1);
		countdownText.text = "1";

		t = 1f;
		while (t > 0) {
			float a = Mathf.Lerp(1, 0, t * 2);
			textColor = new Color(textColor.r, textColor.g, textColor.b, a);
			countdownText.color = textColor;
			t -= Time.deltaTime;
			yield return 0;
		} 

		isAlive = true;

		// Red / GO
		textColor = new Color(1, 38f/255f, 69f/255f, 1);
		countdownText.text = "GO!";

		t = 1f;
		while (t > 0) {
			float a = Mathf.Lerp(1, 0, t);
			textColor = new Color(textColor.r, textColor.g, textColor.b, a);
			countdownText.color = textColor;
			t -= Time.deltaTime;
			yield return 0;
		} 

		countdownText.enabled = false;






	}
}
