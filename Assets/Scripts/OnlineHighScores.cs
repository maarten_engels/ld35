﻿/// <summary>
/// OnlineHighScores.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class OnlineHighScores : MonoBehaviour {

	// private URL: http://dreamlo.com/lb/d31-xPQYHUiQYzISiuhqKQNY0C3hedGUGG74JIQG7KCg
	private const string PRIVATE_CODE = "d31-xPQYHUiQYzISiuhqKQNY0C3hedGUGG74JIQG7KCg";
	private const string PUBLIC_CODE = "57137a986e51b60cec6a82e9";
	private const string URL = "http://dreamlo.com/lb/";

	public static OnlineHighScores Instance;

	public int[] highscores;
	public string playerName = "Dummy"; // <-- we only care about the highest highscore, not about player name.

	void Awake() {
		if (Instance == null) {
			Instance = this; 
		} else {
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
		DownloadHighScores();
	}


	public void AddNewHighscore(int score) {
		StartCoroutine(UploadNewHighScore(score));
	}

	IEnumerator UploadNewHighScore(int score) {
		WWW www = new WWW(URL + PRIVATE_CODE + "/add/" + WWW.EscapeURL(playerName) + "/" + score);
		yield return www;

		if (string.IsNullOrEmpty(www.error)) {
			print("Upload Succesful");
		} else {
			print("Error uploading: " + www.error);
		}
		DownloadHighScores();
	}

	public void DownloadHighScores(int topNumber = 10) {
		StartCoroutine(DownloadHighScoresFromDatabase(topNumber));
	}

	IEnumerator DownloadHighScoresFromDatabase(int topNumber) {
		WWW www = new WWW(URL + PUBLIC_CODE + "/pipe/"+topNumber);
		yield return www;

		if (string.IsNullOrEmpty(www.error)) {
			print(www.text);
			FormatHighscores(www.text);
		} else {
			print("Error downloading: " + www.error);
		}
	}

	void FormatHighscores(string textStream) {
		string[] entries = textStream.Split(new char[] {'\n'}, System.StringSplitOptions.RemoveEmptyEntries);
		highscores = new int[entries.Length];
		for (int i=0; i < entries.Length; i++) {
			string[] entryInfo = entries[i].Split(new char[] {'|'});
			string playerName = entryInfo[0];
			int score = int.Parse(entryInfo[1]);
			highscores[i] = score;
			Debug.Log("Entry: "+i+ " " + playerName + " " + score);
		}
	}

	public int CurrentHighScore {
		get { if (highscores != null && highscores.Length > 0) {
				return highscores[0];
		} else {
			// we assume no highscore has been set yet
			return 0;
		}
	}
	}
}


