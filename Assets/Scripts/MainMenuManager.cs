﻿/// <summary>
/// MainMenuManager.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	public GameObject instructionsPanel;

	public void Start() {

		if (PlayerPrefs.HasKey("InstructionsShown")) {
			instructionsPanel.SetActive(false);
		} else {
			instructionsPanel.SetActive(false);
			PlayerPrefs.SetInt("InstructionsShown", 1);
		}

	}

	public void StartGame(string sceneToLoad) {
		SceneManager.LoadScene(sceneToLoad);
	}

	public void ShowInstructions() {
		instructionsPanel.SetActive(true);
	}

	public void HideInstructions() {
		instructionsPanel.SetActive(false);
	}
	
}
