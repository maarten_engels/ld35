﻿/// <summary>
/// RemoveObstacle.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class RemoveObstacle : MonoBehaviour {

	public GetOutOfTheWay[] objectsToMove;

	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.layer == this.gameObject.layer) {
			foreach (GetOutOfTheWay gotw in objectsToMove) {
				gotw.MoveYo();
			}
		}	
	}

}