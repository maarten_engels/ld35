﻿/// <summary>
/// WinGame.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class WinGame : MonoBehaviour {

	public AudioClip winClip;
	AudioSource audioSource;

	void Start() {
		audioSource = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Player") {

			if (audioSource != null && winClip != null) {
				audioSource.PlayOneShot(winClip);
			}

			GameObject.FindObjectOfType<Player>().Win();
		}
	}

}
