﻿/// <summary>
/// GetOutOfTheWay.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class GetOutOfTheWay : MonoBehaviour {

	public Vector3 moveDistance = Vector3.down;
	public float moveDuration = 1.0f;
	public float openDuration = 2.0f;

	bool isMoving = false;

	public void MoveYo() {
		if (isMoving == false) {
			StartCoroutine("MoveObject");
		}
	}

	IEnumerator MoveObject() {
		isMoving = true;

		float t = moveDuration;
		Vector3 moveSpeed = -moveDistance / moveDuration;

		while (t > 0) {
			transform.Translate(moveSpeed * Time.deltaTime, Space.Self);

			t -= Time.deltaTime;
			yield return null;
		}

		yield return new WaitForSeconds(openDuration);

		t = moveDuration;
		while (t > 0) {
			transform.Translate(-moveSpeed * Time.deltaTime, Space.Self);

			t -= Time.deltaTime;
			yield return null;
		}

		isMoving = false;
	}
}
