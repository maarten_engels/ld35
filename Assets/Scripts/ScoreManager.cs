﻿/// <summary>
/// ScoreManager.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public Text scoreLabel;
	public Text highScoreLabel;
	public Player player;
	[Range(1f,3f)]
	public float difficultyFactor;

	float score = 0;
	float highscore = 0;

	// Use this for initialization
	void Start () {
		highscore = OnlineHighScores.Instance.CurrentHighScore;

		scoreLabel.text = "Score: "+score.ToString("00000000");
		highScoreLabel.text = "Highscore: "+highscore.ToString("00000000");

		player.RegisterGameEndedCallback(WriteHighScore); 
	}
	
	// Update is called once per frame
	void Update () {

		if (player.isAlive) {
			score += Mathf.Pow(player.speed, difficultyFactor) * Time.deltaTime;
			scoreLabel.text = "Score: "+score.ToString("00000000");
		}

		if (score > highscore) {
			highscore = score;
			highScoreLabel.text = "Highscore: "+highscore.ToString("00000000");
		}



	}

	void WriteHighScore() {
		OnlineHighScores.Instance.AddNewHighscore(Mathf.FloorToInt(highscore));
	}

}
