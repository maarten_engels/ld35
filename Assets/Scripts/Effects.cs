﻿/// <summary>
/// Effects.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class Effects : MonoBehaviour {

	AudioSource audioSource;
	public AudioClip bounceClip;

	// Use this for initialization
	void Awake () {
		audioSource = GetComponent<AudioSource>();
	}
	
	void OnCollisionEnter(Collision col) {
		audioSource.PlayOneShot(bounceClip);
	}

}
