﻿/// <summary>
/// Obstacle.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	public AudioClip loseClip;
	AudioSource audioSource;

	void Start() {
		audioSource = GetComponent<AudioSource>();
	}
	
	void OnCollisionEnter(Collision col) {
		if (col.collider.tag == "Player") {

			if (audioSource != null && loseClip != null) {
				audioSource.PlayOneShot(loseClip);
			}

			GameObject.FindObjectOfType<Player>().Die();
		}
	}
}
