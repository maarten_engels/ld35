﻿/// <summary>
/// CreateScreenShot.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class CreateScreenShot : MonoBehaviour {

	private int count = 0;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.P)) {
			count++;
			ScreenCapture.CaptureScreenshot("Screenshot "+ System.DateTime.Now.ToString("yyyyMd hhmmss ")+ count + ".png", 3);
		}

	}
}