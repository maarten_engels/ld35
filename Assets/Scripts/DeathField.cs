﻿/// <summary>
/// DeathField.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using System.Collections;

public class DeathField : MonoBehaviour {

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Player") 
		{
			GameObject.FindObjectOfType<Player>().Die();
		}
	}
}
