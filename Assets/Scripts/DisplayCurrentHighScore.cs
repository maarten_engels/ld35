﻿/// <summary>
/// DisplayCurrentHighScore.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 35
/// License: CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
/// </summary>

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayCurrentHighScore : MonoBehaviour {

	Text text;

	void Start() {
		text = GetComponent<Text>();
	}

	void Update() {

		text.text = "Current Highscore: "+ OnlineHighScores.Instance.CurrentHighScore.ToString("00000000");

	}
}
