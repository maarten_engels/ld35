# Shape Running #
This game was created for Ludum Dare 35, theme: Shapeshift 

So that is just what you do in this runner style game: change shape to match the obstacles so they will let you through. Otherwise...you'll just waltz right into them which you won't survive. 

Controls: use your keyboards numeric keys (above your letter keys) 1 through 4 to change shape. 

Have fun! 

## Created using: ##
* Unity 5; 
* Blender (modeling); 
* Pixelmator (2D stuff); 
* ChipTone (http://sfbgames.com/chiptone/) for SFX; 
* Garageband (music).

## Credits ##
Shape Running v 1.0 (Compo)

(c) Maarten Engels - thedreamweb.eu
This game was created for Ludum Dare 35, licensed by CC BY-NC 4.0
(http://creativecommons.org/licenses/by-nc/4.0/)

**Source Code Pro** font by Adobe, licensed by SIL OPEN FONT LICENSE Version 1.1 (http://scripts.sil.org/OFL). 
Online highscores using Dreamlo (http://dreamlo.com)